extends EnemyAbstract

const COLOR_REGULAR := Color.RED
const COLOR_FREEZE := Color.BLUE
const COLOR_LASER_FREEZE := Color.GREEN

var freeze_timer: float = 0.0
var target: Node3D = null

@onready var material: StandardMaterial3D


func _ready() -> void:
	# gotta give each one a unique material, otherwise they all share the same
	# one and will all be re-colored when only one asks to be
	var mesh := BoxMesh.new()
	mesh.size = Vector3(1.0,1.0,1.0)
	mesh.material = StandardMaterial3D.new()
	material = mesh.material
	($box as MeshInstance3D).mesh = mesh


func _physics_process(delta: float) -> void:
	freeze_timer = maxf(freeze_timer - delta, 0.0)
	if freeze_timer <= 0.0:
		material.albedo_color = COLOR_REGULAR
		if target:
			set_velocity(global_position.direction_to(target.global_position))
			move_and_slide()


func _on_hit(shot_type: StringName) -> bool:
	freeze_timer = 1.0
	if shot_type == &"laser":
		material.albedo_color = COLOR_LASER_FREEZE
	else:
		material.albedo_color = COLOR_FREEZE
	return true


func on_body_entered(body: Node3D) -> void:
	if body is Player:
		target = body.get_node(^"player_collision") as Node3D


func on_body_exited(body: Node3D) -> void:
	if body is Player:
		target = null
