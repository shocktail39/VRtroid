class_name EnemyAbstract
extends CharacterBody3D


# return true if the shot is tangible to the enemy, false if not.  there's a
# difference between being "invincible" and "intangible" to a shot type.
# an invincible enemy will still get hit, just not react to being hit.  this
# should return true.
# an intangible enemy, however, will have the shot pass through them as if
# they're not there.  this should return false.
func _on_hit(_shot_type: StringName) -> bool:
	return false
