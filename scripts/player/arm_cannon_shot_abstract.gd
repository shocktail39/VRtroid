class_name ArmCannonShotAbstract
extends Area3D

var lifespan: float = 0.0
var shot_type: StringName


func _on_hit() -> void:
	pass


func _physics_process(delta: float) -> void:
	var hit_things: Array[Node3D] = get_overlapping_bodies()
	for thing in hit_things:
		if thing is EnemyAbstract:
			# if _on_hit returns false, the shot should pass through as if it
			# didn't hit.
			if (thing as EnemyAbstract)._on_hit(shot_type):
				_on_hit()
		else:
			_on_hit()
	
	lifespan -= delta
	if lifespan <= 0.0:
		queue_free()
