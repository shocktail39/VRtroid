class_name Player
extends CharacterBody3D

enum RelativeMovementOption {HAND, CAMERA}
enum TurnType {SNAP, SMOOTH}

const ROTATE_LOCKOUT: float = 0.25

@export var relative_to := RelativeMovementOption.HAND
@export var turn_type := TurnType.SNAP
@export var turn_sensitivity: float = 0.523598775598 # 30 degrees

var grabbed_object: Node3D
var grabbed_object_position_last_frame: Vector3

var rotate_lockout_timer: float = 0.0
var hook_position: Vector3
var relative_movement_node: Node3D

var walk_movement := WalkMovement.new()
var fall_movement := FallMovement.new()
var grab_movement := GrabMovement.new()
var hook_movement := HookMovement.new()
var current_movement: Movement = walk_movement

@onready var camera := $xr_origin/camera as Node3D
@onready var movement_hand := $xr_origin/movement_hand as XRController3D
@onready var cannon_hand := $xr_origin/cannon_hand as XRController3D
@onready var collision := $player_collision as CollisionShape3D
@onready var grab_area := $xr_origin/movement_hand/grab_area as Area3D
@onready var hook := $xr_origin/movement_hand/hook as RayCast3D
@onready var hook_visual := (
		$xr_origin/movement_hand/hook/hook_visual as MeshInstance3D
)


func _ready() -> void:
	match relative_to:
		RelativeMovementOption.HAND:
			relative_movement_node = movement_hand
		RelativeMovementOption.CAMERA:
			relative_movement_node = camera


func _physics_process(delta: float) -> void:
	# move player collision to the camera,
	# and resize it to how far off the ground the camera is
	collision.position = camera.position
	collision.position.y *= 0.5
	(collision.shape as CylinderShape3D).height = camera.position.y
	
	if movement_hand.is_button_pressed(&"secondary_click"):
		hook.target_position.z -= delta * 8
		hook_visual.position.z = hook.target_position.z * 0.5
		(hook_visual.mesh as CylinderMesh).height = -hook.target_position.z
		hook_visual.visible = true
		hook.force_raycast_update()
		if hook.is_colliding():
			hook_position = hook.get_collision_point()
			current_movement = hook_movement
	else:
		hook.target_position.z = 0.0
		hook_visual.visible = false
	
	var move_stick_position: Vector2 = movement_hand.get_vector2(&"primary")
	move_stick_position.y *= -1.0
	move_stick_position = move_stick_position.rotated(
			-rotation.y - relative_movement_node.rotation.y
	)
	current_movement._move(
			delta,
			move_stick_position,
			movement_hand.is_button_pressed(&"trigger"),
			movement_hand.is_button_pressed(&"primary_click"),
			movement_hand.is_button_pressed(&"grip_click"),
			self
	)
	
	var right_stick_position: float = -cannon_hand.get_vector2(&"primary").x
	if right_stick_position:
		match turn_type:
			TurnType.SNAP:
				rotate_lockout_timer -= delta
				if rotate_lockout_timer <= 0.0:
					rotate_lockout_timer += ROTATE_LOCKOUT
					if right_stick_position < 0.0:
						rotate_player(-turn_sensitivity)
					else:
						rotate_player(turn_sensitivity)
			TurnType.SMOOTH:
				rotate_player(turn_sensitivity * delta * right_stick_position)
	else:
		rotate_lockout_timer = 0.0
	
	if grabbed_object:
		grabbed_object_position_last_frame = grabbed_object.global_position


func rotate_player(radians: float) -> void:
	# the camera position is slightly different from the player node's position,
	# depending on where you are standing in real life.
	# _physics_process ties the collision position to the camera position.
	# by quickly moving the player node to the collision's position before
	# rotating, an edge case is avoided where the player's collision can get
	# rotated off of a ledge.
	translate(collision.position)
	rotate_y(radians)
	translate(-collision.position)
