class_name WalkMovement
extends Movement

const MAX_WALK_SPEED: float = 2.25
const RUN_MULTIPLIER: float = 1.8
const JUMP_SPEED: float = 2.5


func _move(_delta: float, stick: Vector2, jump_button: bool, run_button: bool,
		_grab_button: bool, player: Player) -> void:
	if not player.is_on_floor():
		player.move_and_slide()
		player.current_movement = player.fall_movement
		return
	
	if jump_button:
		player.velocity.y = JUMP_SPEED
		player.move_and_slide()
		player.current_movement = player.fall_movement
		return
	
	stick *= MAX_WALK_SPEED
	if run_button:
		stick *= RUN_MULTIPLIER
	player.velocity.x = stick.x
	player.velocity.z = stick.y
	player.move_and_slide()
