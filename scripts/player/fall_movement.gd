class_name FallMovement
extends Movement

const SOFT_JUMP_SPEED_CAP: float = 2.5
const GRAVITY: float = 3.0
const GRAVITY_ABOVE_JUMP_CAP: float = 6.0


func _move(delta: float, _stick: Vector2, _jump_button: bool, _run_button: bool,
		grab_button: bool, player: Player) -> void:
	if player.is_on_floor():
		player.velocity.y = -0.001
		player.move_and_slide()
		player.current_movement = player.walk_movement
		return
	
	if grab_button and player.grab_area.has_overlapping_bodies():
		player.grabbed_object = player.grab_area.get_overlapping_bodies()[0]
		player.current_movement = player.grab_movement
		return
	
	if player.velocity.y > SOFT_JUMP_SPEED_CAP:
		player.velocity.y -= delta * GRAVITY_ABOVE_JUMP_CAP
	else:
		player.velocity.y -= delta * GRAVITY
	player.move_and_slide()
