extends XRController3D

const CHARGE_SHOT_TIME: float = 1.0
var shot_regular: PackedScene = preload(
		"res://entities/arm_cannon_shot_regular.tscn"
)
var shot_charge: PackedScene = preload(
		"res://entities/arm_cannon_shot_charge.tscn"
)
var shot_laser: PackedScene = preload(
		"res://entities/arm_cannon_shot_laser.tscn"
)
var current_shot_type: PackedScene = shot_regular
var switch_pressed_last_frame: bool = false
var charge_shot_timer: float = 0.0

@onready var root_node: Node = get_tree().current_scene


func fire_shot(shot_type: PackedScene) -> void:
	var shot := shot_type.instantiate() as Node3D
	shot.global_transform = global_transform.translated_local(
			Vector3(0.0, 0.0, -0.5)
	)
	root_node.add_child(shot)


func _physics_process(delta: float) -> void:
	if is_button_pressed(&"secondary_click"):
		# TODO: implement a more intuitive way to switch shots,
		# probably a radial menu in front of the cannon
		if not switch_pressed_last_frame:
			match current_shot_type:
				shot_regular:
					current_shot_type = shot_laser
				shot_laser:
					current_shot_type = shot_regular
		switch_pressed_last_frame = true
	else:
		switch_pressed_last_frame = false
	
	if is_button_pressed(&"trigger"):
		# shoot if timer is 0,
		# as that means the button isn't held from last frame
		if not charge_shot_timer:
			fire_shot(current_shot_type)
		charge_shot_timer += delta
	else:
		if charge_shot_timer >= CHARGE_SHOT_TIME:
			fire_shot(shot_charge)
		charge_shot_timer = 0.0
