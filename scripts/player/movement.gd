class_name Movement
extends Object

# i tried coding all the movement logic into one big _physics_process function.
# turns out doing it like that is a nightmare.  instead, let's try coding it
# more like a finite state machine.  there are several different movement types
# that get switched between, depending on certain conditions.  this way, it's
# much easier to code things like different left stick behavior when walking
# versus falling.


func _move(_delta: float, _stick: Vector2, _jump_button: bool,
		_run_button: bool, _grab_button: bool, _player: Player) -> void:
	return
