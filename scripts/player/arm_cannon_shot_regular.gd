extends ArmCannonShotAbstract

@export var speed: int = 15
@export var type: StringName = &"regular"
var velocity: Vector3


func _ready() -> void:
	lifespan = 1.0
	shot_type = type
	velocity = Vector3.FORWARD * speed


func _physics_process(delta: float) -> void:
	super(delta)
	translate(velocity * delta)


func _on_hit() -> void:
	queue_free()
