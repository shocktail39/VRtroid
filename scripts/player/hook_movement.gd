class_name HookMovement
extends Movement


func _move(delta: float, stick: Vector2, jump_button: bool, run_button: bool,
		grab_button: bool, player: Player) -> void:
	if jump_button:
		player.move_and_slide()
		player.current_movement = player.fall_movement
		return
	
	player.velocity = player.velocity.lerp(
			player.global_position.direction_to(player.hook_position) * 8,
			0.0625
	)
	player.move_and_slide()
	return
