class_name GrabMovement
extends Movement

# since there won't be a hand position for the first frame, pick a default value
# that's way out of anyone's reach: a million meters in every axis.
const NOTHING := Vector3(1000000, 1000000, 1000000)
const BUFFER_SIZE: int = 8
var hand_position_last_frame: Vector3 = NOTHING
var velocity_buffer := PackedVector3Array()


func _init() -> void:
	velocity_buffer.resize(BUFFER_SIZE)
	velocity_buffer.fill(NOTHING)


func _move(delta: float, _stick: Vector2, _jump_button: bool, _run_button: bool,
		grab_button: bool, player: Player) -> void:
	if not grab_button:
		# if we were to just use the current velocity when letting go, then
		# tracking errors could lead to the player getting flung in the wrong
		# direction.  by averaging the velocity over the past few frames, we
		# can make last-frame tracking errors negligable.
		var average_velocity := Vector3.ZERO
		var buffered_count: int = 0
		hand_position_last_frame = NOTHING
		for i in BUFFER_SIZE:
			if velocity_buffer[i] != NOTHING:
				average_velocity += velocity_buffer[i]
				velocity_buffer[i] = NOTHING
				buffered_count += 1
		if buffered_count:
			average_velocity /= buffered_count
		player.velocity = average_velocity
		player.move_and_slide()
		player.current_movement = player.fall_movement
		return
	
	if hand_position_last_frame != NOTHING and delta:
		var offset: Vector3 = (
				hand_position_last_frame - player.grab_area.global_position
		)
		offset -= (
				player.grabbed_object_position_last_frame
				- player.grabbed_object.global_position
		) # this part is important for grabbing moving objects
		var current_velocity: Vector3 = offset / delta
		player.velocity = current_velocity
		player.move_and_slide()
		for i in BUFFER_SIZE - 1:
			velocity_buffer[i] = velocity_buffer[i + 1]
		velocity_buffer[BUFFER_SIZE - 1] = current_velocity
	
	hand_position_last_frame = player.grab_area.global_position
