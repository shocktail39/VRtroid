extends Node3D


func _ready():
	var interface: XRInterface = XRServer.find_interface("OpenXR")
	if interface and interface.is_initialized():
		get_viewport().use_xr = true
	else:
		print("OpenXR not initialised")
